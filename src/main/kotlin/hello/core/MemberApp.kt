package hello.core

import hello.core.member.Grade
import hello.core.member.Member
import hello.core.member.MemberService
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext

fun main() {
    val applicationContext = AnnotationConfigApplicationContext(AppConfig::class.java)

    val memberService = applicationContext.getBean("memberService", MemberService::class.java)
    val member = Member(1L, "memberA", Grade.VIP)
    memberService.join(member)
    println("Member : ${member}")

    val findMember = memberService.findMember(1L)
    println("New Member : ${findMember}")

    println("Member == New Member : ${member == findMember}")
    println("Member === New Member : ${member === findMember}")
}