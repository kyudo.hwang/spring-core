package hello.core

import hello.core.member.MemberServiceImpl
import hello.core.member.Grade
import hello.core.member.Member
import hello.core.order.OrderServiceImpl
import org.springframework.context.annotation.AnnotationConfigApplicationContext

fun main(args: Array<String>) {
    val applicationContext = AnnotationConfigApplicationContext(AppConfig::class.java)
    val memberService = applicationContext.getBean("memberService", MemberServiceImpl::class.java)
    val orderService = applicationContext.getBean("orderService", OrderServiceImpl::class.java)

    val memberId = 1L
    val member = Member(memberId, "memberA", Grade.VIP)
    memberService.join(member)

    val item = "itemA"
    val itemPrice = 10000
    val order = orderService.createOrder(memberId, item, itemPrice)

    println("order = $order")
    println("order.calculatePrice() = ${order.calculatePrice()}")
}
