package hello.core.web

import hello.core.common.MyLogger
import jakarta.servlet.http.HttpServletRequest
import org.springframework.beans.factory.ObjectProvider
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller

class LogDemoController(
    private val logDemoService: LogDemoService,
    private val myLogger: MyLogger
) {

    @RequestMapping("log-demo")
    @ResponseBody
    fun log(request: HttpServletRequest): String {
        val requestURL = request.requestURL.toString()
        myLogger.requestURL = requestURL

        myLogger.log("controller test")
        Thread.sleep(1000)
        logDemoService.logic("test")

        return "OK"
    }
}
