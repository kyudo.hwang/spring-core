package hello.core.discount

import hello.core.annotation.MainDiscountPolicy
import hello.core.member.Member
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component

@Component
//@Qualifier("mainDiscountPolicy")
@MainDiscountPolicy
class FixDiscountPolicy: DiscountPolicy {
    private val discountFixAmount = 1000

    override fun discount(member: Member, price: Int): Int {
        return price - discountFixAmount
    }
}