package hello.core.discount

import hello.core.member.Member

interface DiscountPolicy {
    /**
     * @param member 회원 정보
     * @param price 구매 금액
     * @return 할인 대상 금액
     */
    fun discount(member: Member, price: Int): Int
}
