package hello.core.member

import org.springframework.stereotype.Component

@Component
class MemoryMemberRepository : MemberRepository {
    private val store = HashMap<Long, Member>()

    override fun save(member: Member) {
        store[member.id] = member
    }

    override fun findById(memberId: Long): Member? {
        return store[memberId]
    }
}