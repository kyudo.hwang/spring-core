package hello.core.annotation

import org.springframework.beans.factory.annotation.Qualifier
import java.lang.annotation.Inherited


@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.CLASS, AnnotationTarget.TYPE, AnnotationTarget.TYPE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
@Inherited
@Qualifier("mainDiscountPolicy")
annotation class MainDiscountPolicy()
