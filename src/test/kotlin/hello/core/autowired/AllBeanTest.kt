package hello.core.autowired

import hello.core.AutoAppConfig
import hello.core.discount.DiscountPolicy
import hello.core.member.Grade
import hello.core.member.Member
import hello.core.member.MemberRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class AllBeanTest {
    class DiscountService (
        private val policyMap: Map<String, DiscountPolicy>,
        private val policyList: List<DiscountPolicy>
    ) {
        fun getPolicyMap(): Map<String, DiscountPolicy> {
            return policyMap
        }
        fun getPolicyList(): List<DiscountPolicy> {
            return policyList
        }

        fun discount(member: Member, price: Int, discountCode: String): Int {
            val discountPolicy = policyMap[discountCode] ?: throw IllegalArgumentException("no discount policy for $discountCode")
            return discountPolicy.discount(member, price)
        }
    }


    @Test
    fun findAllBean() {
        val ac = AnnotationConfigApplicationContext(AutoAppConfig::class.java, DiscountService::class.java)
        val discountService = ac.getBean(DiscountService::class.java)
        val member = Member(1L, "userA", Grade.VIP)

        val discountPrice = discountService.discount(member, 10000, "fixDiscountPolicy")

        println(discountPrice)
    }
}