package hello.core.beanfind

import hello.core.AppConfig
import hello.core.member.MemberService
import hello.core.member.MemberServiceImpl
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.NoSuchBeanDefinitionException
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class ApplicationContextBasicFindTest {
    val ac = AnnotationConfigApplicationContext(AppConfig::class.java)

    @Test
    @DisplayName("빈 이름으로 조회")
    fun findBeanByName() {
        val memberService = ac.getBean("memberService")
        Assertions.assertThat(memberService).isInstanceOf(MemberServiceImpl::class.java)
    }

    @Test
    @DisplayName("이름 없이 타입으로 조회")
    fun findBeanByType() {
        val memberService = ac.getBean(MemberService::class.java)
        Assertions.assertThat(memberService).isInstanceOf(MemberService::class.java)
    }

    @Test
    @DisplayName("구체 타입으로 조회")
    fun findBeanByName2() {
        // 구현에 의존한 코드이므로 좋은 코드는 아님
        val memberService = ac.getBean("memberService", MemberServiceImpl::class.java)
        Assertions.assertThat(memberService).isInstanceOf(MemberServiceImpl::class.java)
    }

    @Test
    @DisplayName("빈 이름으로 조회X")
    fun findBeanByNameX() {
        Assertions.assertThatThrownBy {
            ac.getBean("xxxxx", MemberServiceImpl::class.java)
        }.isInstanceOf(NoSuchBeanDefinitionException::class.java)
    }
}