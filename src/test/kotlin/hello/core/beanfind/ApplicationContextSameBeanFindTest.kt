package hello.core.beanfind

import hello.core.member.MemberRepository
import hello.core.member.MemoryMemberRepository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

class ApplicationContextSameBeanFindTest {
    val ac = AnnotationConfigApplicationContext(SameBeanConfig::class.java)

    @Configuration
    class SameBeanConfig {
        @Bean
        fun memberRepository1(): MemberRepository {
            return MemoryMemberRepository()
        }


        @Bean
        fun memberRepository2(): MemberRepository {
            return MemoryMemberRepository()
        }
    }

    @Test
    @DisplayName("타입으로 조회시 같은 타입이 둘 이상 있으면, 중복 오류가 발생한다")
    fun findBeanByTypeDuplicate() {
        Assertions.assertThatThrownBy {
            ac.getBean(MemberRepository::class.java)
        }.isInstanceOf(org.springframework.beans.factory.NoUniqueBeanDefinitionException::class.java)
    }

    @Test
    @DisplayName("타입으로 조회시 같은 타입이 둘 이상 있으면, 빈 이름을 지정하면 된다")
    fun findBeanByName() {
        val memberRepository = ac.getBean("memberRepository1", MemberRepository::class.java)
        Assertions.assertThat(memberRepository).isInstanceOf(MemberRepository::class.java)
    }

    @Test
    @DisplayName("특정 타입을 모두 조회하기")
    fun findAllBeanByType() {
        val memberRepositories = ac.getBeansOfType(MemberRepository::class.java)
        for (key in memberRepositories.keys) {
            println("key = $key value = ${memberRepositories[key]}")
        }
        println("memberRepositories = $memberRepositories")
        Assertions.assertThat(memberRepositories.size).isEqualTo(2)
    }
}