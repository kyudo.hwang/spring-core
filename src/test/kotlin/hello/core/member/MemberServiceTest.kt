package hello.core.member

import hello.core.AppConfig
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class MemberServiceTest {
    private lateinit var memberService: MemberService

    @BeforeEach
    fun setUp() {
        val applicationContext = AnnotationConfigApplicationContext(AppConfig::class.java)
        memberService = applicationContext.getBean("memberService", MemberService::class.java)
    }

    @Test
    fun join() {
        // given
        val member = Member(1L, "memberA", Grade.VIP)

        // when
        memberService.join(member)

        // then
        val findMember = memberService.findMember(1L)
        assert(member == findMember)
    }
}