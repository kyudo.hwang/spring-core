package hello.core.lifecycle

import org.junit.jupiter.api.Test
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

class BeanLifeCycleTest {
    @Configuration
    class LifeCycleConfig {

        @Bean
        fun networkClient(): NetworkClient {
            val networkClient = NetworkClient()
            networkClient.url = "http://hello-spring.dev"
            return networkClient
        }
    }

    @Test
    fun lifeCycleTest() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(LifeCycleConfig::class.java)
        val networkClient = ac.getBean(NetworkClient::class.java)
        ac.close()
    }
}