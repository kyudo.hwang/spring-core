package hello.core.lifecycle

import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy


class NetworkClient {
    var url: String? = null

    init {
        println("생성자 호출, url=$url")
    }


    fun connect() {
        println("connect: $url")
    }

    fun call(url: String): String {
        println("call: $url")
        return "hello $url"
    }

    fun disconnect() {
        println("close: $url")
    }

    @PostConstruct
    fun init() {
        // 의존관계 주입이 끝나면 호출
        connect()
        call("afterPropertiesSet")
    }

    @PreDestroy
    fun close() {
        // 빈이 종료될 때 호출
        disconnect()
    }

}
