package hello.core.configuration

import hello.core.member.MemberRepository
import hello.core.member.MemberService
import hello.core.member.MemberServiceImpl
import hello.core.member.MemoryMemberRepository
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

class ConfigurationTest {

    @Configuration
    class Config {
        @Bean
        fun memberService(): MemberService {
            return MemberServiceImpl(memberRepository())
        }

        @Bean
        fun memberService2(): MemberService {
            return MemberServiceImpl(memberRepository())
        }

        @Bean
        fun memberRepository(): MemberRepository {
            return MemoryMemberRepository()
        }
    }

    @Test
    @DisplayName("memberRepository는 1번 호출되어야 한다")
    fun configurationDeep() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(Config::class.java)
        val memberService = ac.getBean("memberService", MemberService::class.java)
        val memberService2 = ac.getBean("memberService2", MemberService::class.java)

        assert(memberService.getMemberRepository() === memberService2.getMemberRepository())
    }

    @Test
    @DisplayName("getBean은 싱글톤이 보장되어야 한다")
    fun configurationDeep2() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(Config::class.java)
        val memberService = ac.getBean("memberService", MemberService::class.java)
        val memberService2 = ac.getBean("memberService", MemberService::class.java)

        assert(memberService === memberService2)
    }
}