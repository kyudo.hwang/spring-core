package hello.core.scan

import hello.core.AutoAppConfig
import org.junit.jupiter.api.Test

class AutoAppConfigTest {

    @Test
    fun basicScan() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(AutoAppConfig::class.java)
        val beanDefinitionNames = ac.beanDefinitionNames
        for (beanDefinitionName in beanDefinitionNames) {
            val bean = ac.getBean(beanDefinitionName)
            println("name = ${beanDefinitionName} object = ${bean}")
        }
    }
}