package hello.core.order

import hello.core.AppConfig
import hello.core.member.Grade
import hello.core.member.Member
import hello.core.member.MemberService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class OrderServiceTest {
    private lateinit var memberService: MemberService
    private lateinit var orderService: OrderService

    @BeforeEach
    fun setUp() {
        val applicationContext = AnnotationConfigApplicationContext(AppConfig::class.java)
        memberService = applicationContext.getBean("memberService", MemberService::class.java)
        orderService = applicationContext.getBean("orderService", OrderService::class.java)
    }

    @Test
    fun createOrder() {
        // given
        val memberId = 1L
        val member = Member(memberId, "memberA", Grade.VIP)
        memberService.join(member)

        val item = "itemA"
        val itemPrice = 10000

        // when
        val order = orderService.createOrder(memberId, item, itemPrice)

        // then
        println("order = $order")
        println("order.calculatePrice() = ${order.calculatePrice()}")
    }
}