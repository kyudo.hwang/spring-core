package hello.core.discount

import hello.core.AppConfig
import hello.core.member.Grade
import hello.core.member.Member
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.AnnotationConfigApplicationContext

class RateDiscountPolicyTest {
    private lateinit var discountPolicy: RateDiscountPolicy

    @BeforeEach
    fun setUp() {
        val applicationContext = AnnotationConfigApplicationContext(AppConfig::class.java)
        discountPolicy = applicationContext.getBean("discountPolicy", RateDiscountPolicy::class.java)
    }

    @Test
    @DisplayName("VIP는 10% 할인이 적용되어야 한다")
    fun vip_o() {
        // given
        val member = Member(1L, "memberVIP", Grade.VIP)

        // when
        val discount = discountPolicy.discount(member, 1000)

        // then
        assert(discount == 100)
    }

    @Test
    @DisplayName("VIP가 아니면 할인이 적용되지 않아야 한다")
    fun vip_x() {
        // given
        val member = Member(2L, "memberBasic", Grade.BASIC)

        // when
        val discount = discountPolicy.discount(member, 1000)

        // then
        assert(discount == 0)
    }
}