package hello.core.singleton

import org.junit.jupiter.api.Test

class StatefulService {

    fun order(name: String, price: Int): Int {
        println("name = $name, price = $price")
        return price
    }
}