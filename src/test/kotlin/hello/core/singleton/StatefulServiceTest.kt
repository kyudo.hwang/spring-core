package hello.core.singleton

import hello.core.beanfind.ApplicationContextExtendsFindTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.Bean

class StatefulServiceTest {

    class TestConfig {
        @Bean
        fun statefulService() = StatefulService()
    }

    @Test
    fun statefulServiceSingleton() {
        val ac = AnnotationConfigApplicationContext(TestConfig::class.java)
        val statefulService1 = ac.getBean(StatefulService::class.java)
        val statefulService2 = ac.getBean(StatefulService::class.java)

        // A: 사용자 A가 10000원 주문
        val priceA = statefulService1.order("userA", 10000)

        // B: 사용자 B가 20000원 주문
        val priceB = statefulService2.order("userB", 20000)

        Assertions.assertThat(priceA).isEqualTo(10000)
    }
}