package hello.core.singleton

class SingletonService {

    // 1. static 영역에 객체를 딱 1개만 생성
    companion object {
        private val instance = SingletonService()
        fun getInstance(): SingletonService {
            return instance
        }
    }

    // 2. 생성자를 private로 선언하여 외부에서 new 키워드를 사용한 객체 생성을 막음
    private constructor() {}

    fun logic() {
        println("싱글톤 객체 로직 호출")
    }
}