package hello.core.scope

import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.Scope

class SingletonTest {

    @Test
    fun singletonBeanFind() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(SingletonBean::class.java)
        val bean1 = ac.getBean(SingletonBean::class.java)
        val bean2 = ac.getBean(SingletonBean::class.java)

        println("bean1 = $bean1")
        println("bean2 = $bean2")

        ac.close()
        assert(bean1 === bean2)
    }

    @Scope("singleton")
    class SingletonBean {
        init {
            println("SingletonBean.SingletonBean")
        }

        @PostConstruct
        fun init() {
            println("SingletonBean.init")
        }

        @PreDestroy
        fun destroy() {
            println("SingletonBean.destroy")
        }
    }
}