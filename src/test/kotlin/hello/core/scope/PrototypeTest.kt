package hello.core.scope

import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import org.junit.jupiter.api.Test
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.Scope

class PrototypeTest {
    @Test
    fun prototypeBeanFind() {
         val ac = AnnotationConfigApplicationContext(PrototypeBean::class.java)
         val bean1 = ac.getBean(PrototypeBean::class.java)
         val bean2 = ac.getBean(PrototypeBean::class.java)
         println("bean1 = $bean1")
         println("bean2 = $bean2")
         ac.close()

         assert(bean1 !== bean2)
    }

    @Scope("prototype")
    class PrototypeBean {
        init {
            println("PrototypeBean.PrototypeBean")
        }

        @PostConstruct
        fun init() {
            println("PrototypeBean.init")
        }

        @PreDestroy
        fun destroy() {
            println("PrototypeBean.destroy")
        }
    }
}