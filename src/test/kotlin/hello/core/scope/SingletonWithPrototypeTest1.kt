package hello.core.scope

import jakarta.annotation.PostConstruct
import jakarta.annotation.PreDestroy
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.ObjectProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope

class SingletonWithPrototypeTest1 {

    @Test
    fun prototypeFind() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(PrototypeBean::class.java)
        val bean1 = ac.getBean(PrototypeBean::class.java)
        bean1.addCount()
        val bean2 = ac.getBean(PrototypeBean::class.java)
        bean2.addCount()

        val bean3 = ac.getBean(PrototypeBean::class.java)
        bean3.addCount()
        val bean4 = ac.getBean(PrototypeBean::class.java)
        bean4.addCount()

        ac.close()
    }

    @Scope("prototype")
    class PrototypeBean {
        var count = 0

        fun addCount() {
            count++
            println("count = $count")
        }

        @PostConstruct
        fun init() {
            println("PrototypeBean.init $this")
        }

        @PreDestroy
        fun destroy() {
            println("PrototypeBean.destroy")
        }
    }

    @Test
    fun singletonClientUsePrototype() {
        val ac = org.springframework.context.annotation.AnnotationConfigApplicationContext(ClientBean::class.java, PrototypeBean::class.java)
        val clientBean1 = ac.getBean(ClientBean::class.java)
        val clientBean2 = ac.getBean(ClientBean::class.java)
        clientBean1.logic()
        clientBean2.logic()

        ac.close()
    }

    @Scope("singleton")
    class ClientBean(
       private val prototypeBeanProvider: ObjectProvider<PrototypeBean>
    ) {

        fun logic(): Int {
            val prototypeBean = prototypeBeanProvider.getObject()
            prototypeBean.addCount()

            return prototypeBean.count
        }
    }
}